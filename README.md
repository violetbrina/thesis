# Thesis 2019 - Sabrina Rispin - z5075439

### Silhouette Extraction

This project aims to extract accurate silhouettes from phone images to be
used for automatic measurement extraction

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

For image exporting:

- export_fig (by Yair Altman)
- Subaxis - Subplot (by Aslak Grinsted)

For the algorithm:

- MATLAB Image Processing Toolbox
- MATLAB Computer Vision Toolbox
- [Image Graphs](https://www.mathworks.com/matlabcentral/fileexchange/53614-image-graphs) by Steve Eddins
- [Skin Detection](https://au.mathworks.com/matlabcentral/fileexchange/28565-skin-detection) by Gaurav Jain (modified and included)

### Algorithm assumptions

- skin sections are not near the image edge
- non-skin colored background (uses largest skin color cc)
- person is wearing simple (i.e. single color) clothes with few edges/patterns on them
- input should be a directory with images of the specified extension (as entered through the prompts when you run main)

If you would like to test it with annotations please include the json files in
the same directory as the images with the following naming format:

<UNIQUE_IMG_NAME>.jpg

<UNIQUE_IMG_NAME>-Mask<ORIENTATION>.jpg

```
> ls images/
  S001_IMG_0140-MaskSide.json
  S001_IMG_0140.JPG
  S001_IMG_2199-MaskSide.json
  S001_IMG_2199.JPG
  S001_IMG_3788-MaskSide.json
  S001_IMG_3788.JPG
  S001_IMG_4646-MaskArmsOut.json
  S001_IMG_4646.JPG
  S001_IMG_5849-MaskSide.json
  S001_IMG_5849.JPG
  S001_IMG_7731-MaskArmsOut.json
  S001_IMG_7731.JPG
  S001_IMG_7742 2-MaskArmsOut.json
  S001_IMG_7742 2.JPG
  S001_IMG_8291 3-MaskSide.json
  ...
```

### Installing

git clone the current repository and run main.m in MATLAB.

```
> git clone <ssh_link>
> cd thesis/
> ls
  get_settings.m            main.m
  README.md                 SETTINGS.mat
  pipeline_functions/       analysis_functions/
  annotation_functions/
```

Now simply open matlab, navigate to this folder and run main.m

The output will be in a subfolder of the images folder you choose called predictions/
For example, once you've run the code you should see the following in your images
folder:

```
> ls images/
  ...
  S040_IMG_0563.JPG
  S040_IMG_0567-MaskSide.json
  S040_IMG_0567.JPG
  predictions/
  run_stats.txt
```

The run_stats.txt file tells you how many images were found, how many failed
and the total run time.

End with an example of getting some data out of the system or using it for a little demo

## Authors

- **Sabrina Rispin**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

- Hat tip to anyone whose code was used
- Heba Khamis for being an awesome Thesis supervisor
