function [input_dir, image_filenames, annot_filenames, limit] = get_settings()
    % get_settings - retrieves user settings

    % application settings parameters
    load('SETTINGS.mat');
    
    % ask if they would like to use the default parameters
    prompt = {'Would you like to use the default parameters?'};
    f = uifigure;
    use_defaults = uiconfirm(f, prompt, "Load Defaults?", ...
        'Options',{'Yes', 'No','Cancel'}, ...
        'DefaultOption', 1, 'CancelOption', 3);
    delete(f);
       
    % convert to bool and invalid input check
    if strcmp(use_defaults, 'Yes')
        use_defaults = true;
    elseif strcmp(use_defaults, 'No')
        use_defaults = false;
    else
        errordlg(sprintf('Invalid input: %s, Please enter Y/N', use_defaults));
    end
    
    if use_defaults
        input_dir = settings.DEFAULT_INPUT_DIR;
        recursive = settings.DEFAULT_RECURSIVE;
        suffix = settings.DEFAULT_IMAGE_FORMAT;
        limit = settings.IMG_LIMIT;
    else
        uiwait(helpdlg('Please select an directory from which to collect input images',...
        'Select Image Folder'));
        input_dir = uigetdir(settings.DEFAULT_INPUT_DIR);

        % ask if they would like to recursively search that folder of images or not
        prompt = 'Would you like to recursively search for images from this folder? (Y/N):';
        recursive = inputdlg(prompt, 'Recursive Search?', [1], {settings.DEFAULT_RECURSIVE});
        recursive = recursive{1};  % get first input

        % ask what file type the images are
        prompt = 'What image suffix shall I search for?';
        suffix = inputdlg(prompt, 'Image type', [1], {settings.DEFAULT_IMAGE_FORMAT});
        suffix = suffix{1};
        
        % ask for an image run limit
        prompt = 'What is the maximum number of images I should process?';
        limit = inputdlg(prompt, 'Image ,imit', [1], {num2str(settings.IMG_LIMIT)});
        limit = str2num(limit{1});
    end
    
    %% process the settings
    
    % convert to bool and invalid input check
    if recursive == 'Y' || recursive == 'y'
        recursive = true;
    elseif recursive == 'N'|| recursive == 'n'
        recursive = false;
    else
        errordlg(sprintf('Invalid input: %s, Please enter Y/N', recursive));
    end
    
    % set recursive search pattern
    if recursive
        search_str = strcat('**/*.', suffix);
        annot_str = '**/*.json';
        pred_str = strcat('**/*', settings.RESULTS_OUTPUT_SUFFIX);
    else
        search_str = strcat('/*.', suffix);
        annot_str = '*.json';
        pred_str = strcat('*', settings.RESULTS_OUTPUT_SUFFIX);
    end
    
    % search for all of the input images
    image_filenames = dir(fullfile(input_dir, search_str));
    annot_filenames = dir(fullfile(input_dir, annot_str));
    
    % filter out all prediction files
    pred = contains({annot_filenames.name}, settings.RESULTS_OUTPUT_SUFFIX);
    pred_filenames = annot_filenames(pred);
    
    annot_filenames = annot_filenames(~pred);

end

