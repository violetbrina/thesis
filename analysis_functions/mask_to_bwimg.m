function [bw] = mask_to_bwimg(mask)
    % mask_to_bwimg - convert a binary mask to a black and white image
    
    mask_256 = mask * 256;
    bw = cat(3, mask_256, mask_256, mask_256);
end

