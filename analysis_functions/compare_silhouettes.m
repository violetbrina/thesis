function [stats] = compare_silhouettes(predicted_sil, correct_sil)
    %compare_silhouettes - given the predicted and correct silhouette
    %   compute all of the comparison statistics

    % force the same dimensions
    width = max(predicted_sil.Width, correct_sil.Width);
    height = max(predicted_sil.Height, correct_sil.Height);
    
    predicted_mask = annotation_to_mask(predicted_sil, width, height);
    correct_mask = annotation_to_mask(correct_sil, width, height);
    
    stats = struct;
    
    stats.bfscore = bfscore(predicted_mask, correct_mask);
    stats.jaccard = jaccard(predicted_mask, correct_mask);
    stats.dice = dice(predicted_mask, correct_mask);
end
