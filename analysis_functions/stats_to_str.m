function [stats_str] = stats_to_str(stats)
    %stats_to_str - takes in a stats struct and converts to a nice string

    stats_str = sprintf("BFScore: %f, DSC: %f, Jaccard: %f", ...
        stats.bfscore, stats.dice, stats.jaccard);
end
