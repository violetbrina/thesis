function [fig] = output_comparison_image(I, new_annotation, true_annotation, resolution)
    %output_comparison_image - given the predicted and correct silhouette
    %   and the original image create a side by side set of images to
    %   summarise the results
    
    pred_mask = annotation_to_mask(new_annotation, size(I,2), size(I,1));
    
    if ~exist('true_annotation', 'var')
        % only show two images, original and predicted
        
        % start the figure
        fig = figure('Position', resolution, 'visible', 'off');
        hold on;
        
        ax = gca;
        ax.FontSize = 18;
        
        subaxis(1,2,1, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0);
        imshow(I);
        title("Original Image");
        axis tight;
        axis off;

        subaxis(1,2,2, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0);
        imshow(pred_mask);
        title("Predicted Silhouette Mask");
        axis tight;
        axis off;
        
        t = new_annotation.Image;
        sgtitle(t, 'Interpreter', 'none');
        
    else
        % show the original, predicted and actual images
        true_mask = annotation_to_mask(true_annotation, size(I,2), size(I,1));
        
        statstr = stats_to_str(new_annotation.stats);
        
        % start the figure
        fig = figure('Position', resolution, 'visible', 'off');
        hold on;
        
        subaxis(1,3,1, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0);
        imshow(I);
        title("Original Image", 'FontSize', 30);
        axis tight;
        axis off;

        subaxis(1,3,2, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0);
        imshow(pred_mask);
        title("Predicted Silhouette Mask", 'FontSize', 30);
        axis tight;
        axis off;

        subaxis(1,3,3, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0);
        imshow(true_mask);
        title("True Silhouette Mask", 'FontSize', 30);
        axis tight;
        axis off;
        
        t = {new_annotation.Image, statstr};
        sgtitle(t, 'Interpreter', 'none', 'FontSize', 30);
    end

end
