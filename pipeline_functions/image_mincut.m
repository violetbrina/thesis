function final_mask = image_mincut(image, prob_mask, edge_mask, ...
    important_edges, edge_weight)
    %image_mincut
    %   Given an image and the probability mask of each pixel and the edges
    %   of that image (both as binary masks) perform the minimum cut on
    %   that image
    % ARGS:
    % image = MATLAB image object as loaded by imread()
    % edge_mask = binary mask containing the image edges
    % prob_mask = mask with values [0, 1] containing prob of being a
    %             'human' pixel
    % important_edges = edges detected by edge and superpixels
    % edge_weight = edge weight of all non-edges
    
    %% SETUP
    
    % remove extreme weights in prob_mask so prob_mask in [0, 1]
    prob_mask(prob_mask > 1) = 1;
    prob_mask(prob_mask < 0) = 0;
    
    % find all pixels with 0 prob and all 0 neigbours
    nonzero_node_mask = prob_mask >= 1e-10;
    filt = ones(5, 5);
    keep_mask = imfilter(nonzero_node_mask, filt);
    remove_mask = not(keep_mask);
    
    %% STEP 1A - construct the graph -> add edges

    % grayscale image
    image_gs = rgb2gray(image);
    image_double = im2double(image);
    
    % construct image graph
    G = imageGraph(size(image_gs), 4);      % construct graph for the pixel
    
    num_pixels = size(image_gs(:), 1);      % get num_pixels
    pixel_ids = [1:num_pixels]';
    pixel_names = pad(string(pixel_ids), 'left');    % get them as strings
    G.Nodes.Name = pixel_names;
    
    edges = G.Edges.EndNodes;
    
    
    %% STEP 1B - calculate edge weights from pixel neighbour differences
    
    N = length(edges);
    str_len = length(edges{1, 1});
    
    from_strs = reshape([edges{:, 1}], [str_len N])';
    to_strs = reshape([edges{:, 2}], [str_len N])';
    
    from = str2num(from_strs);
    to = str2num(to_strs);
    ind_offset = size(image_gs, 1) * size(image_gs, 2);
    
    from = [image_double(from + 0*ind_offset), ... 
            image_double(from + 1*ind_offset), ...
            image_double(from + 2*ind_offset)];
    to = [image_double(to + 0*ind_offset), ... 
            image_double(to + 1*ind_offset), ...
            image_double(to + 2*ind_offset)];
    
    edgeManhat = sum(abs(from - to), 2);
    edgeEucd = sqrt(sum((from - to) .^ 2, 2));
    edgeProd = sum(from .* to, 2);
    edgeMag = sqrt(sum(from .^ 2, 2) .* sum(to .^ 2, 2));
    edgeCosine = edgeProd ./ edgeMag;   
    
    newWeights = 1 - edgeEucd./max(edgeEucd);
    G.Edges.Weight = newWeights;
    
    % set edge weights for all pixels that are an edge to 0 -> prone to cut        
    combined_edges = important_edges | edge_mask;
    combined_edges(remove_mask == 1) = 0;
    all_edge_ids = pixel_ids(combined_edges(:) == 1);
    graph_edge_indices = findnode(G, all_edge_ids);
    
    N_total = size(graph_edge_indices, 1);
    N_zerocost = sum(sum(important_edges));
%     N_lowcost = sum(sum(edge_mask));
    
    % set to 0 the graph edges that correspond to edges in the image
    zero_edges = zeros(N_zerocost, 4);
%     lowcost_edges = zeros(N_lowcost, 4);
    for i = 1:N_total
        edge_pix_idx = graph_edge_indices(i);
        graph_edges = outedges(G, edge_pix_idx);
        
        % if important edge set cost to 0
        % otherwise just set a very low cost
        if important_edges(edge_pix_idx)
            zero_edges(i, 1:length(graph_edges)) = graph_edges;
%         else
%             lowcost_edges(i, 1:length(graph_edges)) = graph_edges;
        end
    end
    
    % remove any zeros
    zero_edges = zero_edges(:);
    zero_edges = zero_edges(zero_edges ~= 0);
    
%     lowcost_edges = lowcost_edges(:);
%     lowcost_edges = lowcost_edges(lowcost_edges ~= 0);
    
    % set all of those edge weights to 0
    G.Edges{:, 'Weight'} = G.Edges{:, 'Weight'} * edge_weight; % scale by weight
    G.Edges{zero_edges, 'Weight'} = 0;
    
%     g.Edges{lowcost_edges, 'Weight'} = g.Edges{lowcost_edges, 'Weight'} * lowcost_edgeweight;
    
    
    
    %% STEP 1B - construct the graph -> add source and sink
    
    % remove pixels that are 0 from the graph
    rmnodes = find(remove_mask);
    G = rmnode(G, rmnodes);             % rm nodes that are 100% background
    
    % get graph size/num nodes
    N = numnodes(G);  
    
    % add source and sink
    G = addnode(G, {'Source', 'Sink'}); % add source and sink
    source_id = findnode(G, 'Source');  % get source and sink node ids
    sink_id = findnode(G, 'Sink');
    
    pixel_ids = G.Nodes{1:N, 'PixelIndex'};  % go from node id to Pixel IDs
    weights = prob_mask(pixel_ids);          % get edge weights to source
    
    % set any prob == 1 to inf so it's not cut
    source_weights = weights;
    source_weights(source_weights == 1) = inf;
    
    % set any prob == 1 to inf so it's not cut
    sink_weights = 1-weights;
    sink_weights(sink_weights == 1) = inf;
    
    G = addedge(G, source_id, 1:N, source_weights); % add edges to source
    G = addedge(G, sink_id, 1:N, sink_weights); % add edges to sink 

    
    %% STEP 2 - Perfom mincut algorithm on the graph
    
    [MF, GF, CS, CT] = maxflow(G, 'Source', 'Sink');
    
    person_no_source = str2num(vertcat(CS{1:end-1})); 

    %% STEP 3 - new mask from person/background pixels
    
    final_mask = zeros(size(image,1), size(image,2));
    final_mask(person_no_source) = 1;
  
end