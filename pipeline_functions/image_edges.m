function [masked_edges, important_edges, noisy_edges, prob_mask] = image_edges(image, ...
    prob_mask, skin_mask, min_edge_length)
    %image_edges
    %   Given an image find the edges and return the result as a binary
    %   mask
    % ARGS:
    % image = MATLAB image object as loaded by imread()
    % facing = either 'Side' or 'ArmsOut'
    % prob_mask = mask with values [0, 1] containing prob of being a
    %             'human' pixel
    % min_edge_length = threshold for what is considered a 'long' edge
    
    %% STEP 1 - find the edges
    
    % get grayscale image
    image_gs = rgb2gray(image);
    
    % use canny and sobel method to find edges
    [sobel_edges_orig, thres] = edge(image_gs, 'sobel');
    sobel_edges_noisy = edge(image_gs, 'sobel', thres * 0.1, 'vertical');
    canny_edges = edge(image_gs, 'Canny');
    prewitt_edges = edge(image_gs, 'Prewitt');
    roberts_edges = edge(image_gs, 'roberts');

    noisy_edges = sobel_edges_noisy;
    clean_edges = (sobel_edges_orig | canny_edges | prewitt_edges | roberts_edges);


    %% STEP 2A - mask the edges to exclude any where prob_mask == 0
    
    % use prob mask to remove all background pixels (ie where prob == 0)
    potential_pixels = prob_mask;
    potential_pixels(potential_pixels ~= 0) = 1;
    
    masked_clean = clean_edges & potential_pixels;
    masked_noisy = noisy_edges & potential_pixels;
    
    
    %% STEP 2B - fill the background more and remask the edges
    
    % join edges until there are no loose thread
    edges_joined = imclose(masked_clean, strel('disk', 2));
    [bgnd_fill, angle_mask] = background_fill(edges_joined, skin_mask);
    not_bgnd = not(bgnd_fill);
    not_bgnd = imdilate(not_bgnd, strel('disk', 2));
    keep = not_bgnd | skin_mask;

    masked_clean = masked_clean & keep;
    masked_noisy = masked_noisy & keep;
    
    
    %% STEP 3 - find the longest edges for the noisy edges 
    
    masked_noisy_long = longest_edges(masked_noisy, 2000, 1, 0);
    
    
    %% STEP 4 - find all of the important edges using superpixels
    
    [L, ~] = superpixels(image, 10000);
    superpixel_boundaries = boundarymask(L);

    
    %% STEP 5 - combine masked edges and widened_edges into final edge mask
    
    masked_edges = masked_clean;
    important_edges = superpixel_boundaries & masked_clean;
    noisy_edges = masked_noisy_long;
    
    % and remove the background
    prob_mask(not(not_bgnd)) = 0;
    prob_mask = prob_mask + 0.01 * angle_mask;
    
    % set upper and lower mask limits
    prob_mask(prob_mask > 1) = 1;
    prob_mask(prob_mask < 0) = 0;
    
    
end