function new_prob_mask = image_features(image, prob_mask, skin_mask, edge_mask, ...
    noisy_edges, facing, seg_errode, segment_weight, hair_weight, ...
    filled_section_weight, skin_dilate)
    %image_features
    %   Given an image and the probability mask of each pixel and the edges
    %   of that image (both as binary masks) perform the minimum cut on
    %   that image
    % ARGS:
    % image = MATLAB image object as loaded by imread()
    % skin_mask = binary mask of the know skin segments
    % edge_mask = binary mask containing the image edges
    % prob_mask = mask with values [0, 1] containing prob of being a
    %             'human' pixel
    
    % Step 1 - find the torso (or a certain region inside the torso)
    % Step 2 - if ArmsOut remove triangle area under the torso
    % Step 3 - joining segments from edge detection with skin and torso
    
    %% STEP 1 - use human body proportions to find the torso
    
    [torso_mask, torso_idx, torso_data] = find_torso(skin_mask, edge_mask, facing);
    
    % join torso to head and add to skin mask
    top_mask = get_half_mask(skin_mask, 'top');
    top_skin = top_mask & skin_mask;
    
    head_section_mask = sum(torso_mask, 1) ~= 0;
    head_section = top_skin & head_section_mask;

    below_head = not(flood_fill(head_section, 'down'));
    gap = below_head & not(torso_mask);
    gap = imclearborder(gap) & not(head_section);
    gap = imerode(gap, strel('disk', 1));
    gap = bwareafilt(gap, 1);
    
    joined_head_torso = join_sections(head_section | gap | torso_mask, 0.9);
    skin_torso_mask = joined_head_torso | top_skin;
    
    
    %% STEP 2 - if facing is ArmsOut remove triangle area b/w legs
    
    % IF facing is 'ArmsOut' use the edges to cut out in-between leg area
    if strcmp(facing, 'ArmsOut')
        triangle_mask = rm_bottom_triangle(image, skin_mask, ...
            torso_data.bottom, torso_data.midline, 120);
        prob_mask(triangle_mask == 1) = 0;   % update prob mask
    end
    
    
    %% STEP 3 - use the noisy edge set to find the largest segments

    % get the closed segments
    vert_dilate_edges = imdilate(noisy_edges, strel('line', 5, 90));
    segments = not(vert_dilate_edges);
    segments = imclearborder(segments);
    seD = strel('diamond', round(seg_errode/2));
    segments = imerode(segments, seD);
    
    % rm segments found in triangle if found
    if strcmp(facing, 'ArmsOut')
        segments(triangle_mask == 1) = 0;   % update segments
    end
    
    % final errode
    segments = imerode(segments, seD);
    
    % defs for joining the human features
    midline = torso_data.midline;
    
    % rejoin them all together
    if strcmp(facing, 'ArmsOut')
        left_segments = segments; left_segments(:, midline:end) = 0;
        right_segments = segments; right_segments(:, 1:midline-1) = 0;
        
        % initial joins
        seg_left_joined = join_sections(left_segments, 0.9);
        seg_right_joined = join_sections(right_segments, 0.9);
        
        % add the skin and torso
        skin_left = skin_torso_mask; skin_left(:, midline:end) = 0;
        skin_right = skin_torso_mask; skin_right(:, 1:midline-1) = 0;
        seg_skin_left = seg_left_joined | skin_left;
        seg_skin_right = seg_right_joined | skin_right;

        % find head hands mask to mask the segments
        head_hands_mask = get_head_hands_mask(skin_mask, skin_dilate);
        
        seg_skin_left_masked = seg_skin_left & head_hands_mask;
        seg_skin_right_masked = seg_skin_right & head_hands_mask;
        
        % join the top sections
        joined_left_top = join_sections(seg_skin_left_masked, 0.95);
        joined_right_top = join_sections(seg_skin_right_masked, 0.95);
        
        % join to the rest
        joined_left_all = join_sections(joined_left_top | seg_skin_left, 1);
        joined_right_all = join_sections(joined_right_top | seg_skin_right, 1);
        
        % final mask
        seg_skin_joined = joined_left_all | joined_right_all;
        
    elseif strcmp(facing, 'Side')
        % join the segments
        seg_joined = join_sections(segments, 0.9);
        
        % add the skin
        seg_skin_mask = seg_joined | skin_torso_mask;

        % final joined segments mask
        seg_skin_joined = join_sections(seg_skin_mask, 0.7);
    end
    
    %% STEP 4 - find hair
    
    up_shift = 5;
    bbox_size = 200;
    col_thres = 150;
    hair_mask = find_hair(image, skin_mask, up_shift, bbox_size, col_thres);
    
    
    %% STEP 5 - Find filled sections in edges and add weight based on dist
    
    % fill edge_mask and add sections to prob_mask
    vertical_open = imopen(edge_mask, strel('line', 30, 90));
    reconstruct_edges = imreconstruct(vertical_open, edge_mask, 4);
    edges_close = imclose(reconstruct_edges, strel('disk', 20));
    filled_edges = imfill(edges_close, 'holes');
    filled_sections = filled_edges - edge_mask;
    filled_sec_erode = imerode(filled_sections, strel('disk', 2));
    fill_mask = filled_sec_erode + skin_mask;
    
    % compute dist from edges and add distance to prob_mask
    edge_dist = bwdist(edge_mask);
    non_zero_vals = prob_mask ~= 0;
    masked_dist = non_zero_vals .* edge_dist;
    scaled_dist = masked_dist / max(masked_dist(:));
    negative_dist = non_zero_vals .* (1 - scaled_dist);
    filled = imfill(negative_dist, 'holes');
    mask_add = non_zero_vals .* (filled - negative_dist);
    
    
    %% FINAL - update the probability mask
    
    % update prob_mask for the torso
    prob_mask(torso_idx) = prob_mask(torso_idx) + segment_weight;
    
     % add found segments
    prob_mask(segments) = prob_mask(segments) + segment_weight;
    
    % and the joined mask
    prob_mask(seg_skin_joined) = prob_mask(seg_skin_joined) + segment_weight;
    
    % add found hair
    prob_mask(hair_mask) = prob_mask(hair_mask) + hair_weight;
    
    % rm the lower triangle
    if strcmp(facing, 'ArmsOut')
        prob_mask(triangle_mask == 1) = 0;
    end
    
    % create new prob mask using distance weighted points found in step 5
    new_prob_mask = prob_mask;
    new_prob_mask(fill_mask == 1) = new_prob_mask(fill_mask == 1) + ...
        filled_section_weight;
    new_prob_mask = new_prob_mask + mask_add;
    new_prob_mask(new_prob_mask > 1) = 1;
    
    % bound the limits
    new_prob_mask(new_prob_mask > 1) = 1;
    new_prob_mask(new_prob_mask < 0) = 0;
   
end