function [prob_mask, skin_mask] = initial_person_bounding_box(...,
    image, prob_mask, facing, buffer, bb_weight)

    %initial_person_bounding_box
    %   Find an initial bounding box around the person to exclude the
    %   obvious background
    % ARGS:
    % image = MATLAB image object as loaded by imread()
    % filepath = the full filepath to that image
    % facing = either 'Side' or 'ArmsOut', invalid input will result in a
    %          return error
    % buffer = how many pixels of space you will include around the found
    %          skin regions
    % prob_mask = the mask that will be updated with appropriate
    %             probabilities for each pixel in the image whether
    %             the equivalent mapped on pixel from the mask is
    %             the person or not
    
    %% STAGE 1 get skin mask and add to prob_mask
    
    skin_mask = get_skin_mask(image, facing, buffer);
    prob_mask(skin_mask) = 1;
    
    
    %% STAGE 2 get the bounding box of the skin
    
    % update the bounding box
    new_bb = binary_mask_to_bbox(skin_mask);
    
    % convert [x y w h] bb to a binary mask
    [box_mask, box_idx] = bbox_to_binary_mask(new_bb, image, buffer);
    
    
    %% STAGE 3 - add prob of person pixels to all pixels in the final bb
    
    prob_mask(box_idx) = prob_mask(box_idx) + bb_weight;
    
    not_box_idx = true(size(prob_mask));
    not_box_idx(box_idx) = false;
    prob_mask(not_box_idx) = 0;
    

    %% STAGE 3B - make sure the probability mask lies in the range 0-1
    
    prob_mask(prob_mask > 1) = 1;
    prob_mask(prob_mask < 0) = 0;
    

end

