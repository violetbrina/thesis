function [sil] = find_silhouette(image, facing, PROMPT)
    %find_silhouette - finds the human silhouette in a single image object
    %   Executes the find_silhouette function pipeline then returns a
    %   single binary mask that separates out the human and the background
    % ARGS:
    % image = MATLAB image object as loaded by imread()
    % filepath = the full filepath to that image
    % facing = either 'Side' or 'ArmsOut', invalid input will result in a
    %          return error
    % PROMPT = show prompts while running, true or false

    %% Setup
    % load the evidence file
    % contains weights to put for various detected features
    % 1 is 100% certainty of person, 0.5 50% and 0 0%

    % create probability mask
    prob_mask = zeros([size(image, 1), size(image, 2)]);

    % all pipeline settings
    BUFFER = 70;         % buffer around the largest skin section bounding box
    BB_WEIGHT = 0.5;     % weight of pixels in bounding box
    SKIN_DIL = 30;  % amount of skin dilation for armsout update
    MIN_EDGE_LEN = 300;  % min length for what is a 'long' edge
    SEG_ERRODE = 15;     % how much to errode segments found using noisy edges
    SEG_WEIGHT = 0.3;    % weight of large segments found in image_edges
    EDGE_WEIGHT = 50;   % weight of all non-edge edges in mincut graph
    FILLED_SECTION_WEIGHT = 0.45;  % weight of all sections from filled holes edge_mask
    HAIR_WEIGHT = 0.3;   % weight of pixel found to be 'hair'


    %% STEP 1 - remove obvious background using skin detection

    try
        [prob_mask, skin_mask] = initial_person_bounding_box(...
            image, prob_mask, facing, BUFFER, BB_WEIGHT);
    catch
        image = color_adjust(image);
        [prob_mask, skin_mask] = initial_person_bounding_box(...
            image, prob_mask, facing, BUFFER, BB_WEIGHT);
    end

    % IF facing is 'ArmsOut' mask out more of the edges using the skin map
    if strcmp(facing, 'ArmsOut')
        prob_mask = update_probmask_armsout(prob_mask, skin_mask, SKIN_DIL);
    end

    % shows the mask and initial mask - skin regions and bounding area
    if PROMPT
        imshow_alpha(image, prob_mask);
        uiwait(msgbox('Show Next?', 'Close', 'help')), close;
    end

    %% STEP 2 - get the edges from the image

    [edge_mask, important_edges, noisy_edges, prob_mask] = image_edges(...
        image, prob_mask, skin_mask, MIN_EDGE_LEN);

    if PROMPT
        montage([edge_mask, important_edges, noisy_edges, prob_mask])
        uiwait(msgbox('Show Next?', 'Close', 'help')); close;
    end


    %% STEP 3 - find parts of the human body using various features

    prob_mask = image_features(image, prob_mask, skin_mask, edge_mask, ...
        noisy_edges, facing, SEG_ERRODE, SEG_WEIGHT, HAIR_WEIGHT, ...
        FILLED_SECTION_WEIGHT, SKIN_DIL);

    % show updated prob_mask
    if PROMPT
        imshow_alpha(image, prob_mask);
        uiwait(msgbox('Show Next?', 'Close', 'help')); close;
    end


    %% STEP 4 - construct the graph of the image and perform mincut

    if PROMPT
        both = prob_mask + edge_mask;
        both(both > 1) = 1;
        imshow(both);
        uiwait(msgbox('Start Mincut?', 'Close', 'help')); close;
    end

    mincut_mask = image_mincut(image, prob_mask, edge_mask, ...,
        important_edges, EDGE_WEIGHT);

    if PROMPT, uiwait(msgbox('Mincut Complete', 'Close', 'help')), end


    %% DISPLAY RESULTS

    if PROMPT
        imshowpair(prob_mask, mincut_mask, 'montage');
        uiwait(msgbox('Show Next?', 'Close', 'help')); close;

        imshow_alpha(image, mincut_mask);
        uiwait(msgbox('Show Next?', 'Close', 'help')); close;
    end


    %% Post Processing

    % Keep largest section - rm thin sections - close
    edges_rm = imopen(mincut_mask, strel('disk', 2));
    final_mask = bwareafilt(logical(edges_rm), [1000 Inf]);

    if PROMPT
        imshow(final_mask);
        uiwait(msgbox('Show Next?', 'Close', 'help')); close;
    end


    %% Convert the mask to a series of points

    [rows, cols] = find(final_mask);
    k = boundary(cols, rows, 1);

    x = cols(k)';
    y = rows(k)';

    if PROMPT
        plot(x, y), xlim([1 size(image,2)]), ylim([1 size(image,1)]);
        ax = gca;
        ax.YDir = 'reverse';
    end

    sil = cell2struct(num2cell([x; y]), {'x', 'y'});

end
