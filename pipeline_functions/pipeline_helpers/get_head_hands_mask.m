function head_hands_mask = get_head_hands_mask(skin_mask, skin_dilate)
    %get_head_hands_mask
    %   Given the skin_mask for an image return a mask that attaches the 
    %   head and hands/arms
    
    % dilate the mask
    circle = strel('disk', skin_dilate);
    dilated_skin_mask = imdilate(skin_mask, circle);
    
    % find the top and bottom sections
    top = get_half_mask(dilated_skin_mask, 'top');
    top_dil = top & dilated_skin_mask;
    
    % join the top sections
    head_hands_mask = join_sections(top_dil, 0.5);
end

