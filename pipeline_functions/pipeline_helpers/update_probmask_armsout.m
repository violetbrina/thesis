function final_prob_mask = update_probmask_armsout(prob_mask, skin_mask, ...
    skin_dilate)

    %update_probmask_armsout
    %   Given a prob_mask for a 'ArmsOut' image update the prob_mask to be
    %   tighter around the skin sections detected - not just rectangular
    %   bounding box
    
    % dilate the mask
    circle = strel('disk', skin_dilate);
    dilated_skin_mask = imdilate(skin_mask, circle);
    
    % find the top and bottom sections
    top = get_half_mask(dilated_skin_mask, 'top');
    bottom = get_half_mask(dilated_skin_mask, 'bottom');
    
    top_dil = top & dilated_skin_mask;
    bot_dil = bottom & dilated_skin_mask;
    
    % join the top sections
    head_hands_mask = join_sections(top_dil, 0.5);
    
    % join the dilated feet into one section then find bounding box
    feet_joined = join_sections(bot_dil, 0);
    feet_bbox = binary_mask_to_bbox(feet_joined);

    % set the y value to the top
    new_height = feet_bbox(2) + feet_bbox(4);   % y + h
    feet_bbox(2) = 1;                           % y = 1
    feet_bbox(4) = new_height;
    feet_bbox = round(feet_bbox);

    % cut the skin mask
    new_mask = head_hands_mask | bot_dil | feet_joined;
    cutting_mask = bbox_to_binary_mask(feet_bbox, prob_mask, 0);

    cut_mask = new_mask & cutting_mask;

    % add left and right edges of the box
    [x, y, w, h] = feval(@(x) x{:}, num2cell(feet_bbox));
    y_bottom = min(size(skin_mask, 1), y+h);
    x_right = min(size(skin_mask, 2), x+w);
    
    cut_mask(y:y_bottom, x) = true;
    cut_mask(y:y_bottom, x_right) = true;

    % create convex hull and add it to the new mask
    middle_section_mask = imfill(cut_mask, 'holes');

    % remove the left and right edges
    middle_section_mask(y:y_bottom, x) = false;
    middle_section_mask(y:y_bottom, x_right) = false;

    % join the middle section and arms together
    person_mask = new_mask | middle_section_mask;

    % update probability mask so that anything outside this region is 0
    final_prob_mask = prob_mask .* person_mask;
    
end

