function [box_mask, box_indicies] = bbox_to_binary_mask(bb, I, buffer)
    %bbox_to_binary_mask
    %   Given the coordinates of a bounding box return the binary mask of
    %   that box. buffer is the number of pixels around the bb to add
    
    bb = floor(bb);
    x = max(1, bb(1) - buffer) : min(bb(1)+bb(3) + buffer, size(I,2)-1);
    y = max(1, bb(2) - buffer) : min(bb(2)+bb(4) + buffer, size(I,1)-1);
    
    % make the box_mask
    box_mask = zeros([size(I,1) size(I,2)]);
    box_mask(y,x) = 1;
    
    % get box indicies
    box_indicies = find(box_mask == 1);
end

