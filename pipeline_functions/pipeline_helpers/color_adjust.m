function new_image = color_adjust(image)
    %color_adjust
    %   Given an image spread out color dist and return new image
    
    red = histeq(image(:,:,1), 255);
    green = histeq(image(:,:,2), 255);
    blue = histeq(image(:,:,3), 255);
    
    new_image = cat(3, red, green, blue);
end

