function [long_edges_mask] = longest_edges(edge_mask, min_length, dilate_size, errode_size)
    %longest_edges
    %   Given an image edge mask return an edge mask with only the edges
    %   that are of length min_length or greater
    % ARGS:
    % edge_mask = binary 2D mask of the edges
    % min_length = threshold for what is considered a 'long' edge
    
    % dilate a little
    dilated_edges = imdilate(edge_mask, strel('disk', dilate_size));
    
    % find the largest using the cc and sort by largest area
    [labelled_img, ~] = bwlabel(dilated_edges);
    area_measures = regionprops(labelled_img, 'area');
    all_areas = [area_measures.Area];
    [sorted_areas, sort_indexes] = sort(all_areas, 'descend');
    minlen_edges = sort_indexes(sorted_areas > min_length);
    longest_edges = ismember(labelled_img, minlen_edges);
    
    long_edges_mask = longest_edges > 0;
    long_edges_mask = imerode(long_edges_mask, strel('disk', errode_size));
end