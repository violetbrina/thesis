function section_mask = get_half_mask(skin_mask, section)
    %get_half_mask
    %   Given the skin mask the skin in the top half or bottom half
    
    % get the skin mask sections that are in the top part of the body
    left_fill = flood_fill(skin_mask, 'left');
    right_fill = flood_fill(skin_mask, 'right');
    
    % combine left and right to get bars across that are true if there was
    % no skin and fasle if there was
    % then remove all but the largest one
    bars = left_fill & right_fill;
    
    % rm bars touching the top and bottom
    bars_inv = not(bars);
    first_pix = 1;
    last_pix = size(skin_mask, 1) * size(skin_mask, 2);
    bars_middle_inv = imfill(bars_inv, [first_pix; last_pix]);
    bars_middle = not(bars_middle_inv);
    
    % extract the largest one - gap between hands and feet
    bar = bwareafilt(logical(bars_middle), 1);   % get largest bar

    if strcmp(section, 'top')
        section_mask = flood_fill(bar, 'down');
    elseif strcmp(section, 'bottom')
        section_mask = flood_fill(bar, 'up');
    else
        fprintf('Warning: %s is not a valid section\n', section);
    end
end

