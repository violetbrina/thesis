function [final_mask] = rm_bottom_triangle(image, skin_mask, ...
    torso_bottom, torso_middle, down_shift)

    %rm_bottom_triangle - removes the space between the legs using the
    %edge_mask
    % ARGS:
    % image = original input image
    % skin_mask = binary mask of the know skin segments
    % torso_bottom = index of the bottom of the torso
    % torso_middle = index of the middle of the torso
    % down_shift = how far down to shift the top of the triangle
    
    
    %% STEP 1 - detect bottom of triangle using the feet
    
    feet_mask = get_feet_mask(skin_mask, 'ArmsOut');
    feet_hull = join_sections(feet_mask, 0.5);
    feet_hull(feet_mask) = false;
    gap_no_feet = imerode(feet_hull, strel('square', 10));
    between_feet = bwareafilt(logical(gap_no_feet), 1);
    
    
    %% STEP 2 - detect the top of the triangle using corner detection
    
    triangle_region = not(cumprod(between_feet == 0, 1, 'reverse'));
    triangle_region(1:torso_bottom, :) = 0;
    bbox = binary_mask_to_bbox(triangle_region);
    bbox_points = bbox_to_binary_mask(bbox, image, 0);
    
    image_gs = rgb2gray(image);
    corners = detectHarrisFeatures(image_gs);
    
    corners_x = corners.Location(:,1);
    corners_y = corners.Location(:,2);
    in = inpolygon(corners_x, corners_y, bbox_points(:,1), bbox_points(:, 2));
    
    
    %% STEP 3 - check the corners use backup if none, select one if many
    
    pos_points = corners(in);
    final_point = {};
    final_point.x = torso_middle;
    final_point.y = torso_bottom;
    
    if pos_points.Count == 1
        final_point.x = pos_points.Location(1, 1);
        final_point.y = pos_points.Location(1, 2);
    elseif pos_points.Count > 1
        % compute distance to midline then choose closest
        distance = abs(pos_points.Location(:,1) - torso_middle);
        [~, min_idx] = min(distance);
        final_point.x = pos_points.Location(min_idx, 1);
        final_point.y = pos_points.Location(min_idx, 2);
    end
    
    % down shift a little bit to not catch the edge of the legs
    final_point.x = round(final_point.x);
    final_point.y = round(final_point.y + down_shift);
    
    
    %% STEP 4 - fill the triangle
    
    triangle_mask = between_feet;
    triangle_mask(final_point.y, final_point.x) = 1;  % add the top point
    triangle_mask = join_sections(triangle_mask, 0);  % find the boundary
    
    below_triangle = not(cumprod(triangle_mask == 0, 1));  % fill to bottom
    
    final_mask = triangle_mask | below_triangle;
    
    
end

