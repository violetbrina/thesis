function bbox = binary_mask_to_bbox(binary_mask)
    %bb_to_binary_mask
    %   Given the the binary mask of
    %   that box coordinates of a bounding box return . buffer is the number of pixels around the bb to add
    
    convexhull = join_sections(binary_mask, 0);
    
    if sum(sum(binary_mask)) == 0
        disp('Warning: cannot find a bounding box for empty mask')
        bbox = NaN;
    else
        props = regionprops(convexhull, 'BoundingBox');
        bbox = props.BoundingBox;
    end
end
