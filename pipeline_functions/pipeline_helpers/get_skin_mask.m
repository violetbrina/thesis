function [final_skin_mask] = get_skin_mask(image, facing, buffer)

    %get_skin_mask
    %   Get the skin_mask from an image
    % ARGS:
    % image = MATLAB image object as loaded by imread()
    
    %% STAGE 1A - Find the face and arms
    
    % get skin color map
    [~, skin_mask] = generate_skinmap(image);
    
    expanded_skin = imdilate(skin_mask, strel('disk', 30));
    no_border = imclearborder(expanded_skin);
    border_artefacts = expanded_skin - no_border;
    
    skin_no_border_artefacts = skin_mask & not(border_artefacts);
    skin_filt = bwareafilt(logical(skin_no_border_artefacts), 8);
    skin_no_artefacts = imerode(skin_filt, strel('disk', 3));
    
    skin_mask = skin_no_border_artefacts & skin_no_artefacts;
    
    % get the top half mask
    top_section = get_half_mask(skin_mask, 'top');
    
    % then mask the skin mask to keep only the top sections
    top_skin_mask = top_section & skin_mask;
    
    % if facing 'ArmsOut' take the biggest 3 skin segments
    % if facing 'Side' only take 1
    % keep_initial - finding the head and arms, finds the bounding box
    if strcmp(facing, 'Side')
        keep_initial = 1;
    elseif strcmp(facing, 'ArmsOut')
        keep_initial = 3;
    else
        msg = 'Facing "%s" is not "Side" or "ArmsOut", please provide a valid facing';
        error(msg, facing);
    end
    
    % find the largest 'keep_initial' skin regions
    skin_keep = bwareafilt(logical(top_skin_mask), keep_initial); 
        
    % update probability_mask
    final_skin_mask = false(size(top_skin_mask));
    final_skin_mask(skin_keep == 1) = 1;
    

    %% STAGE 1B - Find the bounding box around the face and arms to the floor
    
    % find bb around that largest segment/s
    bb = binary_mask_to_bbox(final_skin_mask);
    
    % drop the bounding box down to the floor
    bb(4) = size(image, 1);  % [x y w h] setting the h (height) to I height
    
    % convert [x y w h] bb to a binary mask
    [box_mask, ~] = bbox_to_binary_mask(bb, image, buffer);
    skin_in_bbox = skin_mask & box_mask;

    
    %% STAGE 2A - Find the feet/ankles within the bounding box
    
    % get the feet section only
    feet_mask = get_half_mask(skin_mask, 'bottom');
    feet_section = skin_in_bbox & feet_mask;
    
    try
        feet = bwareafilt(logical(feet_section), 2);
    catch
        feet = bwareafilt(logical(feet_section), 1);
    end  
        
    % update probability_mask
    final_skin_mask(feet) = 1;
    
end

