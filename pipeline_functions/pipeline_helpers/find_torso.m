function [torso_mask, torso_idx, torso_data] = find_torso(skin_mask, ...
    edge_mask, facing)
    %find_torso
    %   Given an skin_mask and edge_mask find the torso of the person
    % ARGS:
    % skin_mask = binary mask of the know skin segments
    % edge_mask = binary mask containing the image edges
    
    % Step 1A - torso top bottom detection using proportions
    % Step 1B - use dilated edges to find left and right of torso
    
    
    %% STEP 1A - use human body proportions to find the torso
    
    % skin segments contain feet/ankles, head and hand/arms
    [rows, ~] = find(skin_mask);
    top = min(rows);
    bottom = max(rows);
    height = bottom - top;
    bottm_quart = round(top + (bottom - top) * 3/4);
    
    % find the middle, left and right based on EITHER the feet or arms
    if strcmp(facing, 'ArmsOut')
        % just get the feet
        feet_mask = skin_mask;
        feet_mask(1:bottm_quart, :) = false;

        % get mid point from the feet
        [~, columns] = find(feet_mask);
        left = min(columns);
        right = max(columns);
        midline = round(left + (right - left)/2);
    elseif strcmp(facing, 'Side')
        % just get the head/arms
        arms_mask = skin_mask;
        arms_mask(bottm_quart:end, :) = false;

        % get mid point from the feet
        [~, columns] = find(arms_mask);
        left = min(columns);
        right = max(columns);
        midline = round(left + (right - left)/2);
    end
    
    % torso line
    % https://adc.bmj.com/content/90/8/807
    torso_prop = [0.47 0.56];  % SH/H ratio - go with smapp
    torso_bottom = top + round(height .* torso_prop);

    torso_bot = torso_bottom(1);
    torso_mid = round((torso_bot - top)/2);
    torso_top = top + torso_mid;  % use mid line as approx top

    % update height
    torso_height = torso_bot - torso_top;
    
%     imshow(image);
%     hold on;
%     line([1 size(image,2)], [bottom bottom], 'Color', 'r', 'LineWidth', 2);
%     line([1 size(image,2)], [top top], 'Color', 'r', 'LineWidth', 2);
%     line([1 size(image,2)], [bottm_quart bottm_quart], 'Color', 'r', 'LineWidth', 2);
%     line([midline midline], [top bottom], 'Color', 'r', 'LineWidth', 2);
%     line([midline midline], [torso_bottom(2) top], 'Color', 'b', 'LineWidth', 2);
%     line([midline midline], [torso_bottom(1) top], 'Color', 'g', 'LineWidth', 2);
%     line([midline midline], [torso_bot torso_top], 'Color', 'w', 'LineWidth', 2);
% 
%     uiwait(msgbox('Show Next?', 'Close', 'warn')); close;
    
    
    %% STEP 1B - use torso top/bottom and edges to fill the torso
    
    % copy the edges within the torso region
    torso_edges = edge_mask;
    torso_edges(1:torso_top-1, :) = false;
    torso_edges(torso_bot+1:end, :) = false;
    torso_edges(:, midline-10:midline+10) = false;
    
    % dilate slightly
    torso_edges = imdilate(torso_edges, strel('line', 5, 90));
    
    % find top and bottom bounds
    top_line_edges = torso_edges(torso_top, :);
    top_line = imfill(top_line_edges, [1 midline]);
    top_line(top_line_edges) = false;
    
    bot_line_edges = torso_edges(torso_bot, :);
    bot_line = imfill(bot_line_edges, [1 midline]);
    bot_line(top_line_edges) = false;
    
    % use the smallest line region to determine left and right
    columns = find(top_line & bot_line);
    torso_left = min(columns);
    torso_width = max(columns) - min(columns);
   
    % get bounding box mask
    torso_bbox = [torso_left torso_top torso_width torso_height];
    [torso_mask, torso_idx] = bbox_to_binary_mask(torso_bbox, skin_mask, 0);
    
    torso_data = struct(...
        'top', torso_top, 'bottom', torso_bot, ...
        'left', torso_left, 'right', torso_left + torso_width, ...
        'midline', midline);
   
end