function [angle_mask] = angle_fill(edge_mask, angle)
    %angle_fill
    %   Given an edge_mask fill the background from each direction stopping
    %   once you hit the first edge pixel. After rotating the image.
    %   Then rotate back to restore to the original size
    % ARGS:
    % edge_mask = mask with values [0, 1] where 1 signifies an edge
    % skin_mask = binary mask to show skin regions
    
    % pad array and rotate
    n = 400;
    padded_edge = padarray(edge_mask, [n n], 0, 'both');
    rot_edge = imrotate(padded_edge, angle, 'nearest', 'crop');

    % fill from all directions
    rot_up = flood_fill(rot_edge, 'up');
    rot_down = flood_fill(rot_edge, 'down');
    rot_left = flood_fill(rot_edge, 'left');
    rot_right = flood_fill(rot_edge, 'right');

    % combine and rotate back
    rot_mask_all = rot_up | rot_down | rot_left | rot_right;
    fill_padded = imrotate(rot_mask_all, -angle, 'nearest', 'crop');
    angle_mask = fill_padded(n+1:end-n,n+1:end-n);
end