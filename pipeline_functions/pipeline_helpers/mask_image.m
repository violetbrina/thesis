function [masked_image] = mask_image(image, binary_mask);
    %mask_image
    %   Given an image object and a binary mask produce the masked image
   
    % set correct type and set max to 1, min to 0
    binary_mask = uint8(binary_mask);
    binary_mask(binary_mask > 1) = 1;
    binary_mask(binary_mask < 0) = 0;
    
    rgb_mask = cat(3, binary_mask, binary_mask, binary_mask);
    masked_image = image .* rgb_mask;
end

