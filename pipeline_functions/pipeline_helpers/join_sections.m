function [joined_mask] = join_sections(binary_mask, shrinking_factor)
    %join_sections
    %   Given a bw mask join the segments using boundary
    % ARGS:
    % binary_mask = binary mask of a 2D image
    % shrinking_factor = 0 for no shrink, 1 for max shrink
    
    % find all the 1 values rows and columns
    [rows, cols] = find(binary_mask);
    
    % find the boundary
    k = boundary(cols, rows, shrinking_factor);
    
    % get image size
    [M, N] = size(binary_mask);
    
    % convert boundary to mask
    joined_mask = poly2mask(cols(k), rows(k), M, N);
    
end