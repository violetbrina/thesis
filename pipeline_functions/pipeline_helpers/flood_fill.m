function out = flood_fill(mask, direction)
    %flood_fill
    %   Given an edge_mask fill the background from the given direction
    % ARGS:
    % mask = mask with values [0, 1] where 1 signifies an edge
    % direction = 'left', 'right', 'up', 'down'
    
    % make the fill from each direction using cumprod
    if strcmp(direction, 'left')
        out = cumprod(mask == 0, 2);
    elseif strcmp(direction, 'right')
        out = cumprod(mask == 0, 2, 'reverse');
    elseif strcmp(direction, 'up')
        out = cumprod(mask == 0, 1, 'reverse');
    elseif strcmp(direction, 'down')
        out = cumprod(mask == 0, 1);
    else
        fprintf('Warning: %s is not a valid direction\n', direction);
    end
end