function feet_mask = get_feet_mask(skin_mask, facing)
    %get_feet_mask
    %   Given the skin mask find the person's feet and return as a new mask

    % get the bottom half mask
    bottom_section = get_half_mask(skin_mask, 'bottom');
    
    % feet mask is the top two/one region in the keep section
    feet_area = bottom_section & skin_mask;
    
    % find the feet
    cc = bwconncomp(feet_area);
    cc_props = regionprops(cc, 'basic');
    cc_areas = [cc_props.Area];
    [~, sorted_idx] = sort(cc_areas, 'ascend');
    
    if strcmp(facing, 'ArmsOut')
        try
            feet_idx = vertcat(cc.PixelIdxList{sorted_idx(1:2)});
        catch
            feet_idx = vertcat(cc.PixelIdxList{sorted_idx(1)});
        end
    else
        feet_idx = vertcat(cc.PixelIdxList{sorted_idx(1)});
    end
        
    feet_mask = false(size(skin_mask));
    feet_mask(feet_idx) = true;
end

