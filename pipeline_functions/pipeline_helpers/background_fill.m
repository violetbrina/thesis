function [background_mask, angle_mask] = background_fill(edge_mask, ...
    skin_mask)
    %background_fill
    %   Given an edge_mask fill the background from each direction stopping
    %   once you hit the first edge pixel
    % ARGS:
    % edge_mask = mask with values [0, 1] where 1 signifies an edge
    
    % make the fill from each direction using cumprod
    fill_left = flood_fill(edge_mask, 'left');
    fill_right = flood_fill(edge_mask, 'right');
    fill_top = flood_fill(edge_mask, 'down');
    fill_bottom = flood_fill(edge_mask, 'up');
    
    % fill at 45 deg angles left
    angle_mask = false(size(edge_mask));
    frac = 10;
    for angle = 1:180/frac:180
        angle_mask = angle_mask - 1/frac*angle_fill(edge_mask, angle);
    end
    
    angle_mask = 1 + angle_mask;
    
    % create full background_mask
    background_mask = fill_left | fill_right | ...
                      fill_top | fill_bottom;
    
    % keep the largest object
    inverse = not(background_mask) | skin_mask;
    bgnd_largest = bwareafilt(logical(inverse), 1);
    background_mask = not(bgnd_largest);
end