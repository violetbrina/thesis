function [hair_mask] = find_hair(image, skin_mask, upward_shift, ...
    box_size, col_thres)
    %find_hair - finds the hair in the image from the skin_mask
    % ARGS:
    % image = MATLAB image object as loaded by imread()
    % skin_mask = binary mask where the skin is
    % upward_shift = number of pixels above top skin pixel to find hair
    % box_size = size of head bounding box around hair pixel
    % col_thres = threshold of color range to look for hair pixels
    
    % find skin_pixels and locate a hair pixel shifted slightly upwards
    skin_pixels = find(skin_mask);
    [skin_row, skin_col] = ind2sub(size(skin_mask), skin_pixels);
    top = min(skin_row) - upward_shift;
    middle = round(mean(skin_col));
    
    % create the bounding box around the head
    box_size = box_size/2;
    head_mask = false(size(skin_mask));
    y_min = max(-box_size + top, 1);
    y_max = top + box_size;
    x_min = max(-box_size + middle, 1);
    x_max = middle + box_size;
    
    head_mask(y_min:y_max, x_min:x_max) = 1;
    
    % find pixels with a similar color
    hair_pixel = reshape(image(top, middle, :), [3, 1]);
    R = image(:, :, 1) <= hair_pixel(1)+col_thres & ...
        image(:, :, 1) >= hair_pixel(1)-col_thres;
    G = image(:, :, 2) == hair_pixel(2) & ...
        image(:, :, 2) >= hair_pixel(2)-col_thres;
    B = image(:, :, 3) == hair_pixel(3) & ...
        image(:, :, 3) >= hair_pixel(3)-col_thres;
    
    % create the final mask
    hair_pixels = R & G & B & head_mask;
    hair_mask = join_sections(hair_pixels, 0.8);
end

