function imshow_alpha(image, alpha_mask)
    %imshow_alpha
    %   Imshow an image given an alpha matrix
    
    fig = imshow(image);
    set(fig, 'AlphaData', alpha_mask);
end

