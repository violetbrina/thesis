function [annotation] = load_annotation(filepath)
    %load_annotation - given the path to an annotation.json file
    %   read that file into an annotation structured object
    
    annotation = jsondecode(fileread(filepath));
end
