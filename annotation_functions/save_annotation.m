function [filepath] = save_annotation(annotation, directory, filename)
    %save_annotation - given an annotation struct and a directory to save
    %it to, save the annotation as a json file
    
    filepath = fullfile(directory, filename);
    str = jsonencode(annotation);

    fileID = fopen(filepath, 'w');
    fwrite(fileID, str);
    fclose(fileID);
end

