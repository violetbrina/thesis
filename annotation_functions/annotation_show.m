function [mask] = annotation_show(annotation, dimensions)
    % annotation_to_mask - convert a series of x, y points in an annotated
    % silhouette to an image mask
    mask = annotation_to_mask(annotation, dimensions);
    
    figure;
    imshow(mask);
    hold on;
    plot(x, y, 'b','LineWidth', 2);
    hold off;
end

