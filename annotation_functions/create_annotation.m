function [annotation] = create_annotation(sil, image, image_filepath, facing)
    %create_annotation - given the silhouette points of the person
    %   construct an annotation a that can later be saved as a json file
    
    % some variable pre-processing
    [filepath, name, ext] = fileparts(image_filepath);
    
    % create our image annotation object
    annotation = struct;
    annotation.Image = strcat(name, ext);
    annotation.Name = facing;
    annotation.Points = sil;
    annotation.Regions = [];
    annotation.Rotation = 0;
    annotation.Width = size(image, 2);
    annotation.Height = size(image, 1);
    annotation.IsClosed = true;
    annotation.Version = '1.8';
end

