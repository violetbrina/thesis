function [mask] = annotation_to_mask(annotation, width, height)
    % annotation_to_mask - convert a series of x, y points in an annotated
    % silhouette to an image mask
    
    if ~exist('width','var')
        width = annotation.Width;
    end
    
    if ~exist('height','var')
        height = annotation.Height;
    end
    
    x = [annotation.Points.x];
    y = [annotation.Points.y];
    mask = poly2mask(x, y, height, width);
end

