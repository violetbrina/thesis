%% Main Script Parameters

% clear and clean all
clear;
close all;
close all hidden;

% load all helper functions
addpath(genpath('./pipeline_functions'));
addpath(genpath('./annotation_functions'));
addpath(genpath('./analysis_functions'));

% load setting values
load('SETTINGS.mat');

% debug mode or not
PROMPT = false;

% turn off some warnings
warning('off', 'MATLAB:table:RowsAddedExistingVars');


%% Start application - recieving input

% to change program settings see the get_settings function
[input_dir, image_filenames, annot_filenames, img_limit] = get_settings();

% convert annotated images paths to a map
annotation_map = containers.Map;
for i = 1:numel(annot_filenames)
    filename = annot_filenames(i).name;
    F = fullfile(input_dir, annot_filenames(i).name);
    
    % uncomment for normal, comment for validation set
    key = split(filename, '-');       % uncomment for normal
    basename = key{1};                % uncomment for normal
%     [~, basename, ~] = fileparts(F);  % comment for normal

    annotation_map(basename) = F;
end 

% print prompt of how many images found
prompt = sprintf('Found %d images', numel(image_filenames));
uiwait(msgbox(prompt, 'Number of images found', 'help'));

end_at = min(numel(image_filenames), img_limit);
progressbar = waitbar(1/end_at, 'Processing...');

% create output directory
output_dir = strcat(input_dir, settings.RESULTS_DIR);
if ~exist(output_dir, 'dir')
  mkdir(output_dir);
end

% keep an array of failed images
error_images = [];

% start timer
tic

% process each image
for k = 1:end_at
    F = fullfile(input_dir, image_filenames(k).name);
    image = imread(F);
    
    % find the correct annotated silhouette
    [filepath, name, ext] = fileparts(F);
    
    if annotation_map.isKey(name)
        annot_path = annotation_map(name);
        annotation = load_annotation(annot_path);
       
        matches = regexp(annot_path, 'Mask(\w+)\.', 'tokens');
        try
            facing = char(matches{1});
        catch
            fprintf('Could not detect facing for image: %s\n', name);
            error_images = [error_images; {[name, '.JPG']}; {'Unknown'}];
            continue
        end 
    else
        continue;
    end
    
    % if facing is 'ArmsDown' skip
    if strcmp(facing, 'ArmsDown')
        facingName = sprintf('%s-Mask%s.json', name, facing);
        error_images = [error_images; {[name, '.JPG']}; {facingName}];
        continue
    end
    
    % find the silhouette in the image
    msg = sprintf('Finding the silhouette... %.0f%% Done', k/end_at * 100);
    waitbar(k/end_at, progressbar, msg);
    try
        silhouette_points = find_silhouette(image, facing, PROMPT);
    catch
        fprintf('Could not detect silhouette for this image: %s\n', name);
        facingName = sprintf('%s-Mask%s.json', name, facing);
        error_images = [error_images; {[name, '.JPG']}; {facingName}];
        continue
    end
    
    % create new annotation
    waitbar(k/end_at, progressbar, 'Creating and saving annotation...');
    new_annotation = create_annotation(silhouette_points, image, F, facing);
    
    if ~isempty(annotation)
        % generate comparison statistics
        stats = compare_silhouettes(new_annotation, annotation);

        % add statistics to the annotation
        new_annotation.stats = stats;
    end
    
    % save the annotation
    [dir, basename, ext] = fileparts(new_annotation.Image);
    filename = strcat(basename, '-', new_annotation.Name, '-', 'PRED', '.json');
    save_annotation(new_annotation, output_dir, filename);
    
    % generate and save the output image
    res = settings.RESULTS_RESOLUTION;
    fig = output_comparison_image(image, new_annotation, annotation, res);
    
    % uncomment to save skin_mask
%     [~, skin_mask] = generate_skinmap(image);
%     fig = figure('Position', res, 'visible', 'off');
%     imshow(skin_mask);
        
    if strcmp(settings.RESULTS_IMG_FMT, 'fig')
        savefig(strcat(output_dir, name, '-RESULTS.fig'), 'fig');
    elseif strcmp(settings.RESULTS_IMG_FMT, 'jpg')
        export_fig(fig, strcat(output_dir, name, '-PRED.jpg'));
    else
        prompt = "No results format specified, please set settings.RESULTS_FMT to 'jpg' or 'fig'";
        msgbox(prompt, "Error", 'warn');
    end
    close(fig);
    
    % stop if image processing limit has been reached
    if k > img_limit
        msgbox('Image limit has been reached', 'Limit reached', 'warn');
        break
    end
end

% stop timer
ellapsed_time = toc;
total_images = end_at;
total_failed = length(error_images)/2;

% save run stats into a file
fID = fopen([output_dir, '/run_stats.txt'], 'w');
fprintf(fID, 'Run Time: %f min\n', ellapsed_time/60);
fprintf(fID, 'Num Total Images: %d\n', total_images);
fprintf(fID, 'Num Failed Images: %d\n', total_failed);
fprintf(fID, 'Failed Images:\n');
for i = 1:length(error_images)
    fprintf(fID, '%s\n', [error_images{i,1}]);
end
fclose(fID);

% final message
delete(progressbar);
msgbox('Processing Complete', 'Success', 'help');









